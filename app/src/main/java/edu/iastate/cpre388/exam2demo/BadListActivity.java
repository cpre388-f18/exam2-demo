package edu.iastate.cpre388.exam2demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BadListActivity extends AppCompatActivity {
    private LinearLayout badListLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bad_list);

        badListLayout = findViewById(R.id.badListLayout);
        for (int i = 0; i < 10000; i++) {
            addItem(Integer.toString(i));
        }
    }

    private void addItem(String val) {
        TextView newRow = new TextView(this);
        newRow.setText(val);
        badListLayout.addView(newRow);
    }
}
