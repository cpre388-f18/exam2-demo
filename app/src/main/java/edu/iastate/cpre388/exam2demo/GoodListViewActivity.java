package edu.iastate.cpre388.exam2demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class GoodListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_list_view);

        ArrayList<String> listItems = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            listItems.add(Integer.toString(i));
        }
        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                listItems);
        ListView goodListView = findViewById(R.id.goodListView);
        goodListView.setAdapter(listViewAdapter);
    }
}
