package edu.iastate.cpre388.exam2demo;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CAM = 1;
    private ImageView q5ImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        q5ImageView = findViewById(R.id.q5ImageView);
    }

    public void onBadListClick(View v) {
        Intent explicitIntent = new Intent(this, BadListActivity.class);
        startActivity(explicitIntent);
    }

    public void onGoodListClick(View v) {
        Intent explicitIntent = new Intent(this, GoodListViewActivity.class);
        startActivity(explicitIntent);
    }

    public void onCameraClick(View v) {
        Intent camImplicitIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(camImplicitIntent, REQUEST_CAM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CAM:
                if (resultCode == RESULT_OK) {
                    Bitmap result = data.getParcelableExtra("data");
                    q5ImageView.setImageBitmap(result);
                } else {
                    Toast.makeText(this, "User canceled camera", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onURL1Click(View v) {
        Intent implicitIntent = new Intent();
        implicitIntent.setAction(Intent.ACTION_VIEW);
        // The MIME type IS set below.
        implicitIntent.setDataAndType(
                Uri.parse("http://www.ece.iastate.edu/files/2016/10/Fall-Coover.jpg"),
                "image/jpeg");
        startActivity(implicitIntent);
    }

    public void onURL2Click(View v) {
        Intent implicitIntent = new Intent();
        implicitIntent.setAction(Intent.ACTION_VIEW);
        // The MIME type is NOT set below.
        implicitIntent.setData(
                Uri.parse("http://www.ece.iastate.edu/files/2016/10/Fall-Coover.jpg"));
        startActivity(implicitIntent);
    }

    public void onIntentServiceClick(View v) {
        Intent serviceIntent = new Intent(this, Q9Services.PartAIntentService.class);
        startService(serviceIntent);
        startService(serviceIntent);

        new Handler(getMainLooper()).post(
            new Runnable() {
                @Override
                public void run() {
                    JobModel.doWork();
                    Log.d("MainActivity", "Main thread done");
                }
            });
    }

    public void onServiceClick(View v) {
        Intent serviceIntent = new Intent(this, Q9Services.PartBService.class);
        startService(serviceIntent);
        startService(serviceIntent);

        new Handler(getMainLooper()).post(
                new Runnable() {
                    @Override
                    public void run() {
                        JobModel.doWork();
                        Log.d("MainActivity", "Main thread done");
                    }
                });
    }
}
