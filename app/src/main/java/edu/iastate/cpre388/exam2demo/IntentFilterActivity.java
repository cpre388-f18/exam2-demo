package edu.iastate.cpre388.exam2demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class IntentFilterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_filter);

        TextView emailToTextView = findViewById(R.id.emailToTextView);

        // Get the implicit intent given to this Activity
        Intent implicitIntent = getIntent();
        // Get the data from the intent
        String toEmail = getIntent().getDataString();
        // Get the first email address from the intent and display it.
        emailToTextView.setText(getString(R.string.format_email_to, toEmail));
    }
}
