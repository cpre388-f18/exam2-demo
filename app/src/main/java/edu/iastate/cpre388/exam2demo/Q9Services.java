package edu.iastate.cpre388.exam2demo;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

class Q9Services {

    public static class PartAIntentService extends IntentService {
        private static final String TAG = "PartAIntentService";

        public PartAIntentService() {
            super("IntentService");
        }

        @Override
        protected void onHandleIntent(@Nullable Intent intent) {
            Log.d(TAG, "Task start");
            JobModel.doWork();
            Log.d(TAG, "Task done");
        }
    }

    public static class PartBService extends Service {
        private static final String TAG = "PartBService";

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            Log.d(TAG, "Task start");
            JobModel.doWork();
            Log.d(TAG, "Task done");
            return START_NOT_STICKY;
        }

        @Nullable
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }
}
